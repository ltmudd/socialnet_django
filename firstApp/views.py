from django.shortcuts import render

# Create your views here.
def first(request):
    view = 'anon'
    return render(request, 'firstApp/firstpage.html', {'name': view})