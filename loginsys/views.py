# Create your views here.
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from account.forms import ProfileForm
from account.models import Profile
from django.contrib.auth.models import User
from django.db import transaction
from django.contrib.auth.models import User
from .forms import RegisterForm
from django.http import HttpResponse
def get_user_profile(request, username):
    user = User.objects.get(username=username)
    return render(request, '<app_name>/user_profile.html', {"user":user})

@csrf_exempt
def loginup(request):
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect('profile.html', {'user':user})

        else:
            login_error = "user does not found"
            return render(request, 'login.html', {'login_error': login_error})
    else:
        return render(request, 'login.html')

#@login_required
#@transaction.atomic
def register(request):
    print(0)
    if request.POST:
        print(1)
        form = RegisterForm(request.POST)
        print(2)
        if form.is_valid():
            print(2.5)
            form.save()
            print(2.7)
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            print(4)
            user = authenticate(username=username, password=password)
            user.save()
            print(5)
            user.refresh_from_db()  # load the profile instance created by the signal
            user.profile.birth_date = form.cleaned_data.get('birth_date')
            profile_form = ProfileForm(request.POST, instance=user)
            profile_form.save()
            print(6)
            login(request, user)
            return redirect('/profile/'+username+'/detail/') # ,{'form': form,'profile_form': profile_form} можно потом так написать, но это потом, после создания раздела настроек профиля return redirect('settings:profile')
    else:
        form = RegisterForm()
        profile_form = ProfileForm()
    return render(request, 'register.html', {'form': form,'profile_form': profile_form})

def log_out(request):
    logout(request)
    return redirect('/')